/*jslint plusplus: true*/
/*jslint nomen: true*/
/*global $, jQuery, alert, google, console, outdatedBrowser, IScroll, FastClick, videojs*/

var winw, winh, gallery, popgallery, currpage, opacity, scrolling,
  slidecount, currslide, slideoffset, thispage, thisslide,
  is_touch = (typeof (window.ontouchstart) !== "undefined") || (navigator.MaxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0);

$(document).ready(function () {
  "use strict";
  FastClick.attach(document.body);
  window.viewportUnitsBuggyfill.init({
    force: true
  });
  winw = $(window).width();
  winh = $(window).height();
  if ($('.gallery').length) {
    gallery = new IScroll('.gallery', {
      scrollX: true,
      scrollY: false,
      disableMouse: true,
      mouse: false,
      disablePointer: true,
      eventPassthrough: true,
      snap: '.slide',
      momentum: false,
      probeType: 3,
      bounceTime: 100
    });
    popgallery = new IScroll('.pop-gallery', {
      scrollX: true,
      scrollY: false,
      disableMouse: true,
      mouse: false,
      disablePointer: true,
      eventPassthrough: true,
      snap: '.popcont',
      momentum: false,
      probeType: 3,
      bounceTime: 100
    });
    scrolling = false;
    slidecount = $('.slide').length;
    gallery.goToPage(5, 0, 0);
    $('.total').empty().append(slidecount - 6);
    gallery.on('scrollStart', function () {
      scrolling = true;
    });
    gallery.on('scrollEnd', function () {
      scrolling = false;
      if (gallery.currentPage.pageX === slidecount - 3) {
        gallery.goToPage(3, 0, 0);
      } else if (gallery.currentPage.pageX === 2) {
        gallery.goToPage(slidecount - 4, 0, 0);
      }
    });
    popgallery.on('scrollEnd', function () {
      scrolling = false;
      if (popgallery.currentPage.pageX === slidecount - 3) {
        popgallery.goToPage(3, 0, 0);
      } else if (popgallery.currentPage.pageX === 2) {
        popgallery.goToPage(slidecount - 4, 0, 0);
      }
      $('.curr').empty().append(popgallery.currentPage.pageX - 2);
    });
    $('.popup').fadeOut().css({
      "opacity": 1
    });
  }
});

$(document).on('click', '.open, .toggle', function () {
  "use strict";
  $('.nav').toggleClass('open');
});

if (scrolling === false) {
  $(document).on('click', '.desk-arr-right, .popup-arr-right', function () {
    'use strict';
    gallery.next();
    popgallery.next();
  });

  $(document).on('click', '.desk-arr-left, .popup-arr-left', function () {
    'use strict';
    gallery.prev();
    popgallery.prev();
  });
}

$(document).on('click', '.popup-close', function () {
  'use strict';
  $('.popup').fadeOut(200).toggleClass('active');
  $('.gallery').toggleClass('pop');
});

$(document).on('click', '.thumb img', function () {
  'use strict';
  thispage = $(this).parents().eq(1).index();
  thisslide = gallery.currentPage.pageX;
  slideoffset = thispage - thisslide;
  $('.popup').toggleClass('active').fadeIn(200);
  $('.gallery').toggleClass('pop');
  popgallery.goToPage(thispage, 0, 0);
  if (currpage > slidecount - 4) {
    $('.curr').empty().append(slideoffset);
  } else if (thispage < 3) {
    $('.curr').empty().append(slidecount - 5 + slideoffset);
  } else {
    $('.curr').empty().append(thispage - 2);
  }
});

// keyboard navigation
$(document).keydown(function (e) {
  'use strict';
  switch (e.which) {
  case 37: // left
    popgallery.prev();
    gallery.prev();
    break;
  case 39: // right
    popgallery.next();
    gallery.next();
    break;
  default:
    return; // exit this handler for other keys
  }
  e.preventDefault(); // prevent the default action (scroll / move caret)
});
